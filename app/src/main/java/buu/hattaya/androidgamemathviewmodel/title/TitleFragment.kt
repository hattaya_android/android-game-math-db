package buu.hattaya.androidgamemathviewmodel.title

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import buu.hattaya.androidgamemathviewmodel.R
import buu.hattaya.androidgamemathviewmodel.databinding.FragmentTitleBinding

class TitleFragment : Fragment() {
    private lateinit var binding: FragmentTitleBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<FragmentTitleBinding>(inflater,
            R.layout.fragment_title, container, false)
        binding.btnEnterGame.setOnClickListener { view->
            view.findNavController().navigate(R.id.action_titleFragment_to_categoryFragment)
        }
        setHasOptionsMenu(true)
        return binding.root
    }
}